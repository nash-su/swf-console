<?php

use Swf\Console\Attribute\{Command, Argument};
use Swf\Console\{Input, Output, Application};

require dirname(__DIR__) . '/vendor/autoload.php';

$app = Application::instance()->set('app', 'v1.0');

class s {
    #[Command(name: 'http:start', help: 'start', desc: 'http server start')]
    #[Argument(name: 'port', desc: '端口')]
    public static function a(Input $input, Output $output)
    {
        $content = $input->readLine('读取命令行输入');
        $output->writeln('您输入的内容是：' . $content);
    }
}

$app->add(s::class);

$app->run();
