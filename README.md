# swf-console

#### 介绍
命令行交互程序构建框架

#### 软件架构
本框架基于 symfony/console 构建，配置内容使用注解实现，输入内容添加命令行输入读取能力

#### 安装教程
compose require nash-swf/console
