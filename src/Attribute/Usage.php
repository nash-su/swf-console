<?php

namespace Swf\Console\Attribute;

use Attribute;
use Swf\Console\ExecCommand;

#[Attribute(Attribute::TARGET_FUNCTION | Attribute::TARGET_METHOD | Attribute::IS_REPEATABLE)]
class Usage extends ConsoleAttribute
{
    /**
     * @param string $usage
     */
    public function __construct(private string $usage)
    {}

    /**
     * @param ExecCommand $command
     * @return void
     */
    public function __invoke(ExecCommand $command)
    {
        $command->addUsage($this->usage);
    }
}
