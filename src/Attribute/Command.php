<?php

namespace Swf\Console\Attribute;

use Attribute;
use Swf\Console\ExecCommand;

#[Attribute(Attribute::TARGET_FUNCTION | Attribute::TARGET_METHOD)]
class Command extends ConsoleAttribute
{
    public function __construct(private string $name, private string $help = '', private string $desc = '')
    {}

    public function __invoke(ExecCommand $command)
    {
        $command->setName($this->name);
        $command->setDescription($this->desc);
        $command->setHelp($this->help);
    }
}
