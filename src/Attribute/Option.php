<?php

namespace Swf\Console\Attribute;

use Attribute;
use Swf\Console\ExecCommand;
use Symfony\Component\Console\Input\InputOption;

#[Attribute(Attribute::TARGET_FUNCTION | Attribute::TARGET_METHOD | Attribute::IS_REPEATABLE)]
class Option extends ConsoleAttribute
{
    const VALUE_IS_ARRAY = InputOption::VALUE_IS_ARRAY;
    const VALUE_NONE = InputOption::VALUE_NONE;
    const VALUE_OPTIONAL = InputOption::VALUE_OPTIONAL;
    const VALUE_REQUIRED = InputOption::VALUE_REQUIRED;

    /**
     * @param string $name
     * @param string|null $shortcut
     * @param int|null $mode
     * @param string $desc
     * @param mixed|null $default
     */
    public function __construct(private string $name, private ?string $shortcut = null, private ?int $mode = null, private string $desc = '', private mixed $default = null)
    {}

    /**
     * @param ExecCommand $command
     * @return void
     */
    public function __invoke(ExecCommand $command)
    {
        $command->addOption($this->name, $this->shortcut, $this->mode, $this->desc, $this->default);
    }
}
