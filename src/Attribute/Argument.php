<?php

namespace Swf\Console\Attribute;

use Attribute;
use Swf\Console\ExecCommand;
use Symfony\Component\Console\Input\InputArgument;

#[Attribute(Attribute::TARGET_FUNCTION | Attribute::TARGET_METHOD | Attribute::IS_REPEATABLE)]
class Argument extends ConsoleAttribute
{
    const IS_ARRAY = InputArgument::IS_ARRAY;
    const OPTIONAL = InputArgument::OPTIONAL;
    const REQUIRED = InputArgument::REQUIRED;

    /**
     * @param string $name
     * @param int|null $mode
     * @param string $desc
     * @param mixed|null $default
     */
    public function __construct(private string $name, private ?int $mode = null, private string $desc = '', private mixed $default = null)
    {}

    /**
     * @param ExecCommand $command
     * @return void
     */
    public function __invoke(ExecCommand $command)
    {
        $command->addArgument($this->name, $this->mode, $this->desc, $this->default);
    }
}
