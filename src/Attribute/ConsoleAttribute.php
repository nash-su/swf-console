<?php

namespace Swf\Console\Attribute;

use Swf\Console\ExecCommand;

abstract class ConsoleAttribute
{
    abstract public function __invoke(ExecCommand $command);
}
