<?php

namespace Swf\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;

class ExecCommand extends Command
{
    /**
     * @var callable
     */
    private $action;

    /**
     * @param callable $action
     */
    public function __construct(callable $action)
    {
        parent::__construct();
        $this->action = $action;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            call_user_func($this->action, $input, $output);
        } catch (Throwable $exception) {
            echo $exception->getTraceAsString();
            return max(Command::FAILURE, $exception->getCode());
        }
        return Command::SUCCESS;
    }
}
