<?php

namespace Swf\Console;

use Symfony\Component\Console\Formatter\OutputFormatterInterface;
use Symfony\Component\Console\Output\ConsoleOutput;

class Output extends ConsoleOutput
{
    protected static Output $_instance;

    /**
     * @param int $verbosity
     * @param bool|null $decorated
     * @param OutputFormatterInterface|null $formatter
     */
    public function __construct(int $verbosity = self::VERBOSITY_NORMAL, bool $decorated = null, OutputFormatterInterface $formatter = null)
    {
        parent::__construct($verbosity, $decorated, $formatter);
        self::$_instance = $this;
    }

    /**
     * @return static
     */
    public static function instance(): self
    {
        return self::$_instance ?? new self;
    }
}
