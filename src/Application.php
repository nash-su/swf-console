<?php

namespace Swf\Console;

use Exception, ReflectionClass, ReflectionMethod, ReflectionAttribute, ReflectionException, ReflectionFunction;
use Swf\Console\Attribute\Command;
use Swf\Console\Attribute\ConsoleAttribute;
use Symfony\Component\Console\Application as BaseApplication;

final class Application
{
    /**
     * @var Application|null
     */
    protected static ?Application $_instance = null;

    /**
     * @var BaseApplication
     */
    protected BaseApplication $application;

    public function __construct()
    {
        $this->application = new BaseApplication();
        Application::$_instance = $this;
    }

    /**
     * @param string $name
     * @param string $version
     * @return $this
     */
    public function set(string $name, string $version): Application
    {
        $this->application->setName($name);
        $this->application->setVersion($version);
        return $this;
    }

    /**
     * @return BaseApplication
     */
    public function getApplication(): BaseApplication
    {
        return $this->application;
    }

    /**
     * @param string|callable $command
     * @return $this
     * @throws ReflectionException
     */
    public function add(string|callable $command): Application
    {
        if (is_string($command) && class_exists($command))
            $this->application->addCommands(self::getCommands($command));
        elseif (is_array($command))
            $this->application->addCommands(self::getCommands(...$command));
        elseif ($execCommand = self::getMethodCommand($command))
            $this->application->add($execCommand);
        return $this;
    }

    /**
     * @return int
     * @throws Exception
     */
    public function run(): int
    {
        return $this->application->run(new Input(), new Output());
    }

    /**
     * @return Application
     */
    public static function instance(): Application
    {
        return Application::$_instance ?? new self();
    }

    /**
     * @param callable $callback
     * @return ExecCommand|null
     * @throws ReflectionException
     */
    public static function getMethodCommand(callable $callback): ?ExecCommand
    {
        $method = new ReflectionFunction($callback);
        if ($method->getAttributes(Command::class))
            return self::setExecCommand(new ExecCommand($callback), $method->getAttributes());
        return null;
    }

    /**
     * @param string $className
     * @param string|null $methodName
     * @return ExecCommand[]
     * @throws ReflectionException
     */
    public static function getCommands(string $className, ?string $methodName = null): array
    {
        $methodNames = is_null($methodName) ? get_class_methods($className) : [$methodName];
        $classAttributes = ($reflectionClass = new ReflectionClass($className))->getAttributes();
        $methods = $reflectionClass->getMethods(ReflectionMethod::IS_PUBLIC | ReflectionMethod::IS_STATIC);
        $result = [];
        foreach ($methods as $method) {
            if ($method->getAttributes(Command::class) && in_array($methodNow = $method->getName(), $methodNames))
                $result[] = self::setExecCommand(
                    new ExecCommand([$className, $methodNow]), [...$classAttributes, ...$method->getAttributes()]
                );
        }
        return $result;
    }

    /**
     * @param ExecCommand $command
     * @param ReflectionAttribute[] $attributes
     * @return ExecCommand
     */
    protected static function setExecCommand(ExecCommand $command, array $attributes): ExecCommand
    {
        foreach ($attributes as $attribute) {
            if (!class_exists($attribute->getName()))
                continue;
            $attributeAttribute = $attribute->newInstance();
            if (is_subclass_of($attributeAttribute, ConsoleAttribute::class))
                call_user_func($attributeAttribute, $command);
        }
        return $command;
    }
}
