<?php

namespace Swf\Console;

use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Input\InputDefinition;

class Input extends ArgvInput
{
    protected static Input $_instance;

    /**
     * @param array|null $argv
     * @param InputDefinition|null $definition
     */
    public function __construct(array $argv = null, InputDefinition $definition = null)
    {
        parent::__construct($argv, $definition);
        $this->setStream($this->openInputStream());
        self::$_instance = $this;
    }

    /**
     * @return resource
     */
    private function openInputStream()
    {
        return defined('STDIN') ? STDIN : fopen('php://stdin', 'w');
    }

    /**
     * @param string $message
     * @return string
     */
    public function readLine(string $message = ''): string
    {
        Output::instance()->writeln($message);
        return trim(fgets($this->getStream()));
    }

    /**
     * @return static
     */
    public static function instance(): self
    {
        return self::$_instance ?? new self;
    }
}
